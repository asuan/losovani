# Losování

Losování jmen dle prioritizace

Funkce:
Čím větší je číslo u jména tím větší je pravděpodobnost jeho výběru.
Data se ukládají do local storage prohlížeče.
Po výběru je jméno vyřazeno z dalšího slosování.
Údaje u jmen (číslo prioritizace, příznak vyřazení) lze upravovat

Demo URL: https://asuan.gitlab.io/losovani/index.html

JavaScript, HTML