
	var Translator = (function () {
		var instance;

		function createInstance() {
				return new object();
		}
		
		class object {
				constructor() {
						this.lng = 'default';
						this.dictionary = {};
				}

				setLng(lng) {
						this.lng = lng;

						return this.setAppDictionary()
						.then(function() {
							return this.setDictionary();
						}.bind(this));
				}

				setDictionary() {
					return new Promise(function(resolve, reject) {
						var lngPath = 'languages/' + this.lng + '/i18n.js';
						$.getScript(lngPath, () => {
							var dictFunc = 'getDictionary'+ this.lng;
							
							window[dictFunc]()
							.then((dict) => {
								Object.assign(this.dictionary, dict);
								return resolve();
							});
						});
					}.bind(this));
				}

				setAppDictionary() {
					var apps = Config.getInstance().get('apps');

					Object.keys(apps).map((appName) => {
						var app = apps[appName];

						if (app.class && typeof app.class == 'function') {
							var appInstance = new app.class();

							Object.assign(this.dictionary, appInstance.getDictionary());
						}
					});

					return Promise.resolve();
				}

				translate(text) {
					var result = '';

					if (Object.keys(this.dictionary).indexOf(text) >= 0) {
							result = this.dictionary[text];
					}
					else {
							result = "~" + text;
							console.error('Missing translate for word "' + text + '"');
					}

					return result;
				}
				
				translateHtml(html) {
					var translator = Translator.getInstance();
					$(html).find('[translate]').map(function (i, elem) {
						translator.translateElem(elem);
					});
				}

				translateElem(elem) {
					var text = this.translate($(elem).attr('translate').trim());
					var attrs = ['placeholder', 'data-validate', 'title'];
					var attrChanged = false;
				
					attrs.map((attr) => {
						if (typeof $(elem).attr(attr) !== typeof undefined) {
							$(elem).attr(attr, text);
							attrChanged = true;
						}
					});
				
					if (!attrChanged) {
						$(elem).text('');
						$(elem).append(text);
					}
				}
		}

		return {
				getInstance: function () {
						if (!instance) {
								instance = createInstance();
						}
						return instance;
				}
		};
})();