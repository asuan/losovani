/* Inits */
$(document).ready(function () {
	InitPage();
});

function InitPage() {
	var lng = getUrlParameter('lng') || Settings.getInstance().get('lng') || Config.getInstance().get('defaultLanguage');

	InitLanguages(lng);
	InitApp();

	$("#clear-dialog-confirm").hide();
	$('#pager button').map(function(i, elem) {
			$(elem).on('click', onSwitchPage.bind(this));
	});
	$('.page').bind('beforeShow', beforeShowPage.bind(this));
	$('form .validate-input input').map(function(i, elem) {
			$(elem).on('focus', onFocusInput.bind(this));
			$(elem).on('blur', onValidate.bind(this));
	});
	$('form button').map(function(i, elem) {
			$(elem).on('click', onButtonClick.bind(this));
	});

	switchPage('add');
}

function InitApp() {
	var settings = Settings.getInstance().getAll();
	var apps = Config.getInstance().get('apps');
	var appName = settings.appName;
	
	if (settings && appName) {
		var app = apps[appName];

		if (app.class && typeof app.class == 'function') {
			var appInstance = new app.class();

			appInstance.setSettings(settings[appName]);
			appInstance.run();
		}
	}
}

/* Name list */
function listName(form) {
	var list = List.getInstance();
	var table = $(form).find('.wrap-table table')[0];
	var names = list.getList();
	var maxProbability = 0;

	names.map(function(name) {
		if (name.disabled) {
			name.priority = 0;
		}
		maxProbability += name.priority;
	});

	names.sort((a, b) => (a.priority < b.priority) ? 1 : (a.priority === b.priority) ? ((a.name > b.name) ? 1 : -1) : -1 )

	names.map(function(name) {
		var probability = (name.priority/maxProbability)*100;

		if (probability > 0 && probability < 1) {
			probability = Math.round(probability * 100)/100;
		}
		else {
			probability = Math.round(probability);
		}
		
		var tr = $('<tr data-name="' + name.name + '"' + (name.disabled?' class="disabled"':'') + '>' +
				'<td class="name">' + name.name + '</td>' +
				'<td class="probability">' + probability + '%</td>' +
		'</tr>');
		$(table).append(tr);
		tr.on('click', onChangeName.bind(this));
	});
}
function clearListName(form) {
		var table = $(form).find('.wrap-table table')[0];
		var trs = $(table).children();

		trs.map(function(i, tr) {
				$(tr).remove();
		});
}
function getName(form) {
	var name = {name: '', priority: 0, disabled: false};
	var inputs = $(form).find('input');

	inputs.map(function (i, input) {
		var type = $(input).attr('type');
		var key = $(input).attr('name');
		var val = $(input).val().trim();

		if (type == 'checkbox') {
			val = $(input).is(':checked');
		}
		else if (type == 'number') {
			val = val.replace(/\s/g, '');
		}
		
		name[key] = val;
	});

	return name;
}
function addName(form) {
	var list = List.getInstance();
	var name = getName(form);

	alert(form, list.addToList(name), 'name-add');
}
function changeName(form) {
	var list = List.getInstance();
	var name = getName(form);

	alert(form, list.changeList(name), 'name-change');
	clearListName(form);
	listName(form);
}
function removeName(form) {
	var list = List.getInstance();
	var name = getName(form);

	alert(form, list.removeFromList(name), 'name-remove');
	clearListName(form);
	listName(form);
}
function choiceName(form) {
	var list = List.getInstance();
	var translator = Translator.getInstance();
	var names = list.getList();
	var inputs = $(form).find('input');
	var choiceList = [];
	var alertMsg = '';

	alertClear(form)
	
	names.map(function(name, nameId) {
		if (!name.disabled) {
			for (var i = 0; i < name.priority; i++) {
				choiceList.push(nameId);
			}
		}
	});

	if (choiceList.length > 0) {
		var randomId = Math.floor(Math.random() * choiceList.length);
		var nameId = choiceList[randomId];

		inputs.map(function (i, input) {
			var key = $(input).attr('name');
			var val = names[nameId][key];

			$(input).val(val);
		});

		names[nameId].disabled = true;

		if (list.changeList(names[nameId])) {
			alertMsg = '<span class="alert-ok">' + translator.translate('name-choice-ok') + '</span>';
		}
		else {
			alertMsg = '<span class="alert-fail">' + translator.translate('name-choice-fail') + '</span>';
		}
	}
	else {
		alertMsg = '<span class="alert-fail">' + translator.translate('name-choice-empty') + '</span>';
	}

	$(form).find('.form-alert').append(alertMsg);
	setTimeout(function () { alertClear(form) }, 1000*10);
}

/* Dynamic load JS file */
function loadjscssfile(filename, filetype) {
	if (!checkjscssfile(filename, filetype)) {
		if (filetype=='js'){ //if filename is a external JavaScript file
			var fileref=document.createElement('script')
			fileref.setAttribute('type','text/javascript')
			fileref.setAttribute('src', filename)
		}
		else if (filetype=='css'){ //if filename is an external CSS file
			var fileref=document.createElement('link')
			fileref.setAttribute('rel', 'stylesheet')
			fileref.setAttribute('type', 'text/css')
			fileref.setAttribute('href', filename)
		}
		if (typeof fileref!='undefined')
			$('head')[0].appendChild(fileref)
	}

	return checkjscssfile(filename, filetype);
}
function checkjscssfile(filename, filetype) {
	var check = false;
	var files = [];
	var pathAttr = 'null';

	if (filetype=='js'){ //if filename is a external JavaScript file
		files = $('head script');
		pathAttr = 'src';
	}
	else if (filetype=='css'){ //if filename is an external CSS file
		files = $('head link[rel=stylesheet]');
		pathAttr = 'href';
	}

	files.map(function(i, file) {
		var path = $(file).attr(pathAttr);

		if (path && path == filename) {
			check = true;
		}
	});

	return check;
}
				
/* Languages */
function InitLanguages(lng) {
	var elem = $('#languages');
	var select = elem.find('select')
	var languages = Config.getInstance().get('languages');

	if (Object.keys(languages).indexOf(lng) < 0) {
		lng = Config.getInstance().get('defaultLanguage');
	}

	fillSelect(select, lng);
	$(select).on('change', onChangeLng.bind(this));
	setLanguage(lng);
}
function setLanguage(lng) {
	// Set translations
	var translator = new Translator.getInstance();
	translator.setLng(lng)
	.then(function() {
		translator.translateHtml($('body'));
	});;

	// Set design
	if(lng && lng != '') {
		var designPath = 'languages/' + lng + '/styles.css';
		loadjscssfile(designPath, 'css');
	}
}
function onChangeLng(event) {
	event.preventDefault();
	event.stopPropagation();

	var elem = event.currentTarget;
	var lng = $(elem).val();
	
	Settings.getInstance().set('lng', lng);
	window.location.href = getUrlParameter('url') + '?lng=' + lng;
}
function getUrlParameter(sParam) {
	var sPageURL = window.location.href,
			URL = sPageURL.split('?')[0],
			sURLSearch = sPageURL.split('?')[1],
			sURLVariables = (sURLSearch?sURLSearch.split('&'):[]),
			sParameterName,
			i;

	if(sParam === 'url') {
		return URL;
	}
	else {
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
	}
};

/* Page */
function onSwitchPage(event) {
	event.preventDefault();
	event.stopPropagation();

	var button = event.currentTarget;
	var pageId = $(button).attr('class').split(' ').filter(function (className) {
		if (className.trim().match(/^switch-/g)) {
			return true;
		}
		return false;
	});

	pageId = pageId[0].replace(/^switch-/g, '') || '';

	if (pageId) {
		switchPage(pageId);
	}
}
function switchPage(pageId) {
	$('.page').map(function (i, page) {
		if ($(page).attr('id') == pageId) {
			showPage(page);
		}
		else {
			$(page).hide();
		}
	});
}
function showPage(page) {
	$(page).trigger('beforeShow');
	$(page).show();
}
function beforeShowPage(event) {
	var page = event.currentTarget;
	var form = $(page).children('form');
	var pageId = $(page).attr('id');

	switch (pageId) {
		case 'add':
			break;
		case 'list':
			clearForm(form);
			clearListName(form);
			listName(form);
			break;
		case 'choice':
			clearForm(form);
			break;
		case 'settings':
			var select = $(form).find('.wrap-select select[name=streamApp]')[0];
			fillSelect(select);
			$(select).trigger('change');
			break;
		default: break;
	}
}

/* Form */
/* Validate */
function onFocusInput(event) {
		event.preventDefault();
		event.stopPropagation();

		var input = event.currentTarget;
		
		hideValidate(input);
		$(input).parent().removeClass('true-validate');
}
function onValidate(event) {
		event.preventDefault();
		event.stopPropagation();

		var input = event.currentTarget;
		
		if(!validate(input)){
				showValidate(input);
		}
		else {
				$(input).parent().addClass('true-validate');
		}
}
function validate (input) {
	if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
		if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
			return false;
		}
	}
	else if($(input).attr('type') == 'number') {
		var min = parseFloat($(input).attr('min')) || Number.NEGATIVE_INFINITY;
		var max = parseFloat($(input).attr('max')) || Number.POSITIVE_INFINITY;
		var val = parseFloat($(input).val());
		
		if(val < min || val > max) {
			return false;
		}
	}
	else {
		if($(input).val().trim() == ''){
			return false;
		}
	}
	return true;
}
function showValidate(input) {
	var thisAlert = $(input).parent();

	$(thisAlert).addClass('alert-validate');

	$(thisAlert).append('<span class="btn-hide-validate">&#xf135;</span>')
	$('.btn-hide-validate').each(function(){
		$(this).on('click',function(){
			hideValidate(this);
		});
	});
}
function hideValidate(input) {
	var thisAlert = $(input).parent();
	$(thisAlert).removeClass('alert-validate');
	$(thisAlert).find('.btn-hide-validate').remove();
}
function showPassword(event) {
	event.preventDefault();
	event.stopPropagation();

	var elem = event.currentTarget;
	var td = $(elem).parents('td')[0];
	var passwordElem = $(td).find('input[type=password]')[0];
	var textElem = $(td).find('input[type=text]')[0];

	if (passwordElem) {
		$(passwordElem).attr('type', 'text');
		$(elem).addClass('hover');
	}
	else if (textElem) {
		$(textElem).attr('type', 'password');
		$(elem).removeClass('hover');
	}
}

/* Form */
/* Submit */
function onSubmit(event) {
	event.preventDefault();
	event.stopPropagation();

	var check = true;
	var button = event.currentTarget;
	var form = $(button).parents('form');
	var pageId = $(button).parents('.page').attr('id');
	var inputs = $(form).find('.validate-input input');
	var action = $(button).attr('action');

	inputs.map(function (i, input) {
		if(!validate(input)){
			showValidate(input);
			check=false;
		}
	});

	if (check) {
		switch (pageId) {
			case 'add':
				addName(form);
				break;
			case 'list':
				if (action === 'change') {
					changeName(form);
				}
				else if (action === 'delete') {
					removeName(form);
				}
				break;
			case 'choice':
				choiceName(form);
				break;
			case 'settings':
				Settings.getInstance().saveForm(form);
				break;
			default: break;
		}
	}
}
function onButtonClick(event) {
		event.preventDefault();
		event.stopPropagation();

		var button = event.currentTarget;
		var pageId = $(button).parents('.page').attr('id');
		var action = $(button).attr('action');

		if (['submit', 'delete', 'change'].indexOf(action) >= 0) {
				return onSubmit(event);
		}

		if (action === 'show-password') {
				return showPassword(event);
		}

		if (pageId == 'list' && action === 'clear') {
			var dialogElem = $("#clear-dialog-confirm");
			var labelDelete = Translator.getInstance().translate('clear-dialog-delete');
			var labelCancel = Translator.getInstance().translate('clear-dialog-cancel');
			var dialogButtons = {};

			dialogButtons[labelDelete] = function() {
				$(dialogElem).dialog("close");
				clearListName($(button).parents('form'));
				List.getInstance().clearList();
			};
			dialogButtons[labelCancel] = function() {
				$(dialogElem).dialog("close");
			};

			$(dialogElem).dialog({
				resizable: false,
				height: "auto",
				width: 450,
				modal: true,
				buttons: dialogButtons
			});
		}
}
function onChangeName(event) {
		event.preventDefault();
		event.stopPropagation();
		
		var list = List.getInstance().getList();
		var tr = event.currentTarget;
		var changeName = $(tr).attr('data-name');
		var nameId = -1;
		var form = $(tr).parents('form');
		var inputs = $(form).find('input');

		list.map(function(name, i) {
				if (changeName == name.name) {
						nameId = i;
				}
		});

		inputs.map(function (i, input) {
				var type = $(input).attr('type');
				var key = $(input).attr('name');
				var val = list[nameId][key];

				hideValidate(input);

				if (type == 'checkbox') {
						$(input).prop('checked', !!val);
				}
				else {
						$(input).val(val);
				}
		});
}

/* Currencies */
function currencyLoad(baseCurrency) {
	var storage = Storage.getInstance().getStorage();

	return new Promise(function(resolve, reject) {
		var currencyList = storage.getItem('currencyList');

		if (!baseCurrency) {
			return reject('No base currency selected.');
		}

		if (!currencyList) {
			currencyList = '{}';
		}

		currencyList = JSON.parse(currencyList) || {};

		if (currencyList[baseCurrency] && Date.parse(currencyList[baseCurrency].date + 'T23:59:59') >= Date.now()) {
			return resolve(currencyList[baseCurrency]);
		}

		$.ajax({
			url: 'https://api.exchangeratesapi.io/latest?base=' + baseCurrency,   
			dataType: 'json',
			success: function(json) {
				currencyList[json.base] = json;
				storage.setItem('currencyList', JSON.stringify(currencyList));

				return resolve(currencyList[baseCurrency]);
			}
		});
	});
}
function currencyConvert(from, to, amount) {
		return new Promise(function(resolve, reject) {
				currencyLoad(to).then(function(currencyList) {
						var converted = Math.round(amount / currencyList.rates[from]);
						
						return resolve(converted);
				});
		});
}

/* Others */
function fillSelect(elem, selectedValue) {
	if ($(elem).prop('name') == 'streamApp') {
		Settings.getInstance().fillStreamAppSelect(elem);
	}
	else if ($(elem).prop('name') == 'lng') {
		var languages = Config.getInstance().get('languages');
	
		$(elem).text('');
		Object.keys(languages).map((key) => {
			$(elem).append('<option class="wrap-select" value="' + key + '"' + (selectedValue==key?' selected':'') + '>' + languages[key] + '</option>');
		});
	}
}
function clearForm(form) {
	var inputs = $(form).find('input');

	inputs.map(function (i, input) {
		var type = $(input).attr('type');

		if (type == 'checkbox') {
			$(input).prop('checked', false);
		}
		else {
			$(input).val('');
		}
	});
}
function alert(form, isOk, msg) {
	var translator = Translator.getInstance();
	var alertMsg = '';
	if (isOk) {
		alertMsg = '<span class="alert-ok">' + translator.translate(msg + '-ok') + '</span>';
		clearForm(form);
	}
	else {
		alertMsg = '<span class="alert-fail">' + translator.translate(msg + '-fail') + '</span>';
	}
	$(form).find('.form-alert').append(alertMsg);
	setTimeout(function () { alertClear(form) }, 1000*10);
}
function alertClear(form) {
	$(form).find('.form-alert').text('');
}