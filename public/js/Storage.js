
		var tmpStorage = {
            storage: {},

            getItem: function(key) {
                    return this.storage[key];
            },
            setItem: function(key, val) {
                    this.storage[key] = val;
            },
            removeItem: function(key) {
                    this.storage[key] = null;
            },
            clear: function() {
                    var allCookies = this.storage;
                    Object.keys(allCookies).map(function(key) {
                            this.removeItem(key);
                    });
            },
            length: function(key) {
                    return this.storage[key].length;
            }
    }

    var Storage = (function () {
        var instance;
    
        function createInstance() {
                return new object();
        }
        
        class object {
            constructor() {
                this.storage = null;
                        
                try {
                    const key = "testStorage";
                    localStorage.setItem(key, key);
                    localStorage.removeItem(key);
                    this.storage = localStorage;
                } catch (e) {
                    this.storage = tmpStorage;
                }
            }
    
            getStorage() {
                return this.storage;
            }
        }
    
        return {
            getInstance: function () {
                if (!instance) {
                    instance = createInstance();
                }
                return instance;
            }
        };
    })();