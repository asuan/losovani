var Settings = (function () {
	var instance;

	function createInstance() {
			return new object();
	}
	
	class object {
		constructor() {
			this.storage = Storage.getInstance().getStorage();
			this.storageName = 'settings';
			
			$('form .wrap-select select[name=streamApp]').map((i, elem) => {
				$(elem).on('change', this.onChangeApp.bind(this));
			});
		}

		getAll() {
			if (this.storage) {
				var settings = this.storage.getItem(this.storageName) || '{}';
		
				return JSON.parse(settings);
			}
			else {
				return {};
			}
		}

		get(key) {
			var settings = this.getAll();
			var setting = settings[key] || null;

			return setting;
		}

		set(key, val) {
			var settings = {};

			settings[key] = val;
			this.save(settings);

			return true;
		}

		saveForm(form) {
			var translator = Translator.getInstance();
			var result = false;
			var settings = this.getAll();
			var streamAppElem = $(form).find('[name=streamApp]')[0];
			var appName = $(streamAppElem).val();
			var apps = Config.getInstance().get('apps');
			var alertMsg = '';
		
			if (settings && this.storage) {
				settings.appName = appName;
		
				Object.keys(apps).map(function(key) {
					var app = apps[key];
		
					if (app.class && typeof app.class == 'function') {
						var appInstance = new app.class();
		
						if (key == appName) {
							settings[key] = appInstance.saveSettings(form);
						}
						else {
							settings[key] = {};
							appInstance.stop();
						}
					}
				});
		
				if (this.save(settings)) {
					alertMsg = '<span class="alert-ok">' + translator.translate('settings-ok') + '</span>';
					result = true;
				}
				else {
					alertMsg = '<span class="alert-fail">' + translator.translate('settings-fail') + '</span>';
					result = false;
				}
			}
			else {
				alertMsg = '<span class="alert-fail">' + translator.translate('settings-fail') + '</span>';
				result = false;
			}
		
			$(form).find('.form-alert').append(alertMsg);
			setTimeout(function () { alertClear(form); }, 1000*10);

			return result;
		}

		save(newSettings) {
			var settings = this.getAll();
			var result = false;
		
			if (settings && this.storage) {
				Object.keys(newSettings).map((key) => {
					var val = newSettings[key];
	
					settings[key] = val;
				});
				this.storage.setItem(this.storageName, JSON.stringify(settings));
				result = true;
			}
			else {
				result = false;
			}

			return result;
		}

		onChangeApp(event) {
			event.preventDefault();
			event.stopPropagation();
		
			var elem = event.currentTarget;
			var streamAppSettingsElem = $(elem).parents('form').find('.streamAppSettings')[0];
			var apps = Config.getInstance().get('apps');
			var appName = $(elem).val();
		
			$(streamAppSettingsElem).text('');
			
			var app = apps[appName] || {};

			if (typeof app.class == 'function') {
				var translator = Translator.getInstance();
				var appInstance = new app.class();
				var html = appInstance.getHtml();
				
				translator.translateHtml(html);
				$(html).find('.validate-input input').map(function(i, elem) {
					$(elem).on('focus', onFocusInput.bind(this));
					$(elem).on('blur', onValidate.bind(this));
				});
				$(html).find('button').map(function(i, elem) {
						$(elem).on('click', onButtonClick.bind(this));
				});
				$(streamAppSettingsElem).append(html);
			}
		}

		fillStreamAppSelect(elem) {
			var translator = Translator.getInstance();
			var current = this.get('appName');
			var apps = Config.getInstance().get('apps');
			
			$(elem).text('');
			Object.keys(apps).map(function(val) {
				var option = $('<option value="' + val + '"' + (val==current?' selected':'') + ' translate="' + apps[val].name + '"></option>');
				translator.translateElem(option);
				$(elem).append(option)
			});
		}
	}

	return {
		getInstance: function () {
			if (!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();