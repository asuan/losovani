/*!
 * StreamLab v1.0.1
 * Copyright 2020 AsuanWeb (https://gitlab.com/asuan/)
 * Licensed under MIT
*/

if (typeof jQuery === 'undefined') {
	throw new Error('StreamLab\'s JavaScript requires jQuery. jQuery must be included before StreamLab\'s JavaScript.')
}

(function ($) {
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1)) {
    throw new Error('StreamLab\'s JavaScript requires at least jQuery v1.9.1 or v2.9')
  }
})(jQuery);

var StreamLab = (function () {
		var instance;

		function createInstance() {
				return new object();
		}
		
		class object {
				constructor() {
						this.socket = null;
						this.eventPromise = Promise.resolve();
						this.types = ['donation', 'follow', 'subscription'];
						this.currencies = ['CZK', 'EUR', 'USD'];
						this.platforms = ['twitch', 'youtube', 'mixer'];

						this.settings = {
								token: '',
								eventType: '',
								baseCurrency: '',
								EURperBit: 0.0153,
								platforms: this.platforms
						};
				}

				setSettings(settings) {
					if(settings.token && settings.token != '') { this.settings.token = settings.token; }
					if(settings.eventType && settings.eventType != '') { this.settings.eventType = settings.eventType; }
					if(settings.baseCurrency && settings.baseCurrency != '') { this.settings.baseCurrency = settings.baseCurrency; }
					if(settings.EURperBit && parseFloat(settings.EURperBit) > 0) { this.settings.EURperBit = parseFloat(settings.EURperBit); }
					if(settings.platforms) { this.settings.platforms = settings.platforms; }
				}

				run() {
					return new Promise((resolve, reject) => {
						this.reconnect()
						.then(() => {
							//Perform Action on event
							this.socket.on('event', (eventData) => {
								const type = this.settings.eventType;
								var promise = Promise.resolve();
			
								if (['donation', 'superchat', 'bits'].indexOf(eventData.type) >= 0) {
									if (type == 'donation') {
										promise = this.eventDonation(eventData);
									}
								}
								else if (['follow'].indexOf(eventData.type) >= 0) {
									if (type == 'follow') {
										promise = this.eventFollow(eventData);
									}
								}
								else if (['subscription'].indexOf(eventData.type) >= 0) {
									if (type == 'subscription') {
										promise = this.eventSubscription(eventData);
									}
								}
			
								this.eventPromise = this.eventPromise.then(() => {
									promise.then(function(name) {
										if (name && name.name != "") {
											List.getInstance().addToList(name);
										}
										return Promise.resolve();
									})
									.catch(function(msg) {
										console.error(msg);
									});
								});
							});
							
							this.socket.on('disconnect', () => {
								console.log('Disconnected from websocket');
								console.log(Date.now()/1000);
								this.reconnect();
							});
			
							return resolve();
						});
					})
					.catch(function(msg) {
							console.error(msg);
					});
				}

				reconnect() {
					return new Promise((resolve, reject) => {
						const socketToken = this.settings.token;
		
						if (!socketToken) {
							return reject('No StreamLab socket token. Skipped.');
						}
						
						//Connect to socket
						this.socket = io(`https://sockets.streamlabs.com?token=${socketToken}`, {transports: ['websocket']});
		
						// Socket connected
						this.socket.on('connect', () => {
							console.log('Successfully connected to the websocket');
							console.log(Date.now()/1000);
							return resolve();
						});
					})
				}

				stop() {
					if (this.socket) {
						this.socket.off();
						this.socket.close();
					}
				}

				eventDonation(eventData) {
					return new Promise(function(resolve, reject) {
						var name = {name: "", priority: 0, disabled: false};
						var baseCurrency = this.settings.baseCurrency;
						var type = eventData.type;
						var payload = eventData.message[0] || {};

						if (!baseCurrency) {
							return reject('No base currency!');
						}

						if (['bits'].indexOf(type) >= 0) {
							//code to handle bits events
							type = 'donation';
							
							// Bits to EUR
							payload.amount = payload.amount * this.settings.EURperBit;
							payload.currency = 'EUR';
						}

						if (['donation', 'superchat'].indexOf(type) >= 0) {
							if (type == 'superchat') {
								payload.amount = payload.amount / 1000000;
							}
							//code to handle donation events
							name.name = payload.name;
							if (payload.currency == baseCurrency) {
								name.priority = payload.amount;
								return resolve(name);
							}
							else {
								var from = payload.currency;
								var to = baseCurrency;
								var amount = payload.amount;

								currencyConvert(from, to, amount).then(function(amount) {
									name.priority = amount;
									return resolve(name);
								});
							}
						}
					}.bind(this));
				}

				eventFollow(eventData) {
					return new Promise(function(resolve, reject) {
						var name = {name: "", priority: 1, disabled: false};
						var list = List.getInstance().getList(name);
						var platforms = this.settings.platforms;
						var platform = eventData.for.replace(/_account$/g, '');
						var payload = eventData.message[0] || {};
						var nameId = -1;

						if (platforms.indexOf(platform) >= 0) {
							name.name = payload.name;
							
							// Filter unique names
							list.map(function(n, i) {
								if(n.name == name.name) {
									nameId = i;
								}
							});
				
							if (nameId < 0) {
								return resolve(name);
							}
						}

						return resolve(null);
					}.bind(this));
				}

				eventSubscription(eventData) {
					return new Promise(function(resolve, reject) {
						var name = {name: "", priority: 1, disabled: false};
						var platforms = this.settings.platforms;
						var platform = eventData.for.replace(/_account$/g, '');
						var payload = eventData.message[0] || {};
						
						if (platforms.indexOf(platform) >= 0) {
							if (typeof payload.gifter == 'string') {
								name.name = payload.gifter_display_name || payload.gifter;
							}
							else {
								name.name = payload.display_name || payload.name;
							}

							if (platform == 'twitch') {
								name.priority = payload.sub_plan / 1000;
							}
						}

						return resolve(name);
					}.bind(this));
				}

				getHtml() {
					var eventTypeSelect = $('<select class="input100" type="text" name="eventType"></select>');
					this.types.map((key) => {
						var selected = (this.settings.eventType==key?' selected':'');
						var option = $('<option value="' + key + '" translate="' + key + '"' + selected + '></option>');
						eventTypeSelect.append(option)
					});
					
					var eventPlatformSelect = $('<select class="input100 select2" type="text" name="platforms[]" multiple="multiple"></select>');
					this.platforms.map((key) => {
						var selected = (this.settings.platforms.indexOf(key) >= 0?' selected':'');
						var option = $('<option value="' + key + '" translate="' + key + '"' + selected + '></option>');
						eventPlatformSelect.append(option)
					});
					
					var baseCurrencySelect = $('<select class="input100" type="text" name="baseCurrency"></select>');
					this.currencies.map((key) => {
						var selected = (this.settings.baseCurrency==key?' selected':'');
						var option = $('<option value="' + key + '" translate="' + key + '"' + selected + '></option>');
						baseCurrencySelect.append(option)
					});

					var html = $('' +
					'<tbody>' +
						'<tr>' +
							'<td>' +
								'<label class="input-label m-b-20" translate="StreamLab_Token-label"></label>' +
								'<a href="https://dev.streamlabs.com/docs/getting-started" class="form-btn m-t-7 m-r-10 float-r hov-pointer button-help" target="blank">?</a>' +
							'</td>' +
							'<td>' +
								'<div style="display: block; float: left; width: 83%;">' +
									'<div class="wrap-input100 validate-input m-b-20" translate="StreamLab_Token-err" data-validate="">' +
										'<input class="input100" translate="StreamLab_Token" type="password" name="token" placeholder="" value="' + this.settings.token + '">' +
										'<span class="focus-input100"></span>' +
									'</div>' +
								'</div>' +
								'<button class="form-btn m-t-7 m-r-0 button-show" action="show-password" style="float: right;"><img src="images/icons/showhide.png" /></button>' +
							'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>' +
								'<label class="input-label m-b-20" translate="StreamLab_Type-label"></label>' +
							'</td>' +
							'<td>' +
								'<div class="wrap-input100 m-b-20">' +
									'<div class="wrap-select">' +
										eventTypeSelect.wrapAll('<div>').parent().html() +
									'</div>' +
									'<span class="focus-input100"></span>' +
								'</div>' +
							'</td>' +
						'</tr>' +
						'<!--tr>' +
							'<td>' +
								'<label class="input-label m-b-20" translate="StreamLab_Platform-label"></label>' +
							'</td>' +
							'<td>' +
								'<div class="wrap-input100 m-b-20">' +
									'<div class="wrap-select">' +
										eventPlatformSelect.wrapAll('<div>').parent().html() +
									'</div>' +
									'<span class="focus-input100"></span>' +
								'</div>' +
							'</td>' +
						'</tr-->' +
						'<tr>' +
							'<td>' +
								'<label class="input-label m-b-20" translate="StreamLab_Currency-label"></label>' +
							'</td>' +
							'<td>' +
								'<div class="wrap-input100 m-b-20">' +
									'<div class="wrap-select">' +
										baseCurrencySelect.wrapAll('<div>').parent().html() +
									'</div>' +
									'<span class="focus-input100"></span>' +
								'</div>' +
							'</td>' +
						'</tr>' +
						'<tr>' +
							'<td>' +
								'<label class="input-label m-b-20" translate="StreamLab_Bits-label"></label>' +
							'</td>' +
							'<td>' +
								'<div class="wrap-input100 validate-input m-b-20" translate="StreamLab_Bits-err" data-validate="">' +
									'<input class="input100" translate="StreamLab_Bits" type="number" name="EURperBit" placeholder="" value="' + this.settings.EURperBit + '" min="0.0001" step="0.0001">' +
									'<span class="focus-input100"></span>' +
								'</div>' +
							'</td>' +
						'</tr>' +
					'</tbody>');

					$(html).find('select.select2').map((i, select) => {
						$(select).select2();
					});

					return html;
				}
				
				saveSettings(form) {
					var appSettingsElem = $(form).find('.streamAppSettings');
					var inputs = $(appSettingsElem).find('.input100');
					
					this.stop();

					inputs.map(function (i, input) {
						var type = $(input).attr("type");
						var key = $(input).attr("name");
						var val = $(input).val();

						if (type == 'checkbox') {
							this.settings[key] = $(input).is(':checked');
						}
						else {
							this.settings[key] = val;
						}
					}.bind(this));

					this.run();
					
					return this.settings;
				}
				
				getDictionary() {
					return {
						'StreamLab': 'StreamLab',
						'StreamLab_Token-label': 'socketToken',
						'StreamLab_Token-err': 'socketToken is not set',
						'StreamLab_Token': 'socketToken',
						'StreamLab_Type-label': 'Event',
						'StreamLab_Currency-label': 'Currency',
						'StreamLab_Platform-label': 'Platform',
						'StreamLab_Bits-label': 'Price 1 Twitch Bit',
						'StreamLab_Bits-err': 'Price must be non zero.',
						'StreamLab_Bits': 'With "." (dot) decimal separator',
						'donation': 'Donation',
						'follow': 'Follow',
						'subscription': 'Subscription',
					};
				}
		}

		return {
				getInstance: function () {
						if (!instance) {
								instance = createInstance();
						}
						return instance;
				}
		};
})();
