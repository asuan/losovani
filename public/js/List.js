
var List = (function () {
	var instance;

	function createInstance() {
			return new object();
	}
	
	class object {
		constructor() {
			this.storage = Storage.getInstance().getStorage();
			this.storageName = 'nameList';
		}

		getList() {
			if (this.storage) {
				var list = this.storage.getItem(this.storageName);
				var names = JSON.parse(list) || [];

				names.map(function(name) {
					name.priority = parseFloat(name.priority);
				});

				return names;
			}
			else {
				/* Test data */
				return [
					{name: "a", priority: 10, disabled: false},
					{name: "b", priority: 20, disabled: false},
					{name: "c", priority: 30, disabled: false},
					{name: "d", priority: 40, disabled: false},
					{name: "e", priority: 50, disabled: true},
					{name: "f", priority: 60, disabled: false}
				];
			}
		}
		
		saveList(list) {
			if (this.storage) {
				if (!list) {
					list = [];
				}

				this.storage.setItem(this.storageName, JSON.stringify(list));

				return true;
			}
			return false;
		}
		
		addToList(name) {
			var list = this.getList();
			var nameId = -1;
			console.log(name);
			
			list.map(function(n, i) {
					if(n.name == name.name) {
							nameId = i;
					}
			});

			if (nameId >= 0) {
					list[nameId].priority = list[nameId].priority + parseFloat(name.priority);
			}
			else {
					list.push(name);
			}

			if (this.saveList(list)) {
				var form = $('#list form');
				
				clearListName(form);
				listName(form);

				return true;
			}

			return false;
		}
		
		clearList() {
			if (this.storage) {
				this.storage.removeItem(this.storageName);

				return true;
			}
			return false;
		}
		
		changeList(name) {
				var result = true;
				result &= this.removeFromList(name);
				if (result) {
						result &= this.addToList(name);
				}
				return result;
		}
		
		removeFromList(name) {
				var list = this.getList();
				var nameId = -1;
				
				list.map(function(n, i) {
						if(n.name == name.name) {
								nameId = i;
						}
				});

				if (nameId >= 0) {
						list.splice(nameId, 1);
						return this.saveList(list);
				}

				return false;
		}
	}

	return {
		getInstance: function () {
			if (!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();