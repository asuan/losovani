var Config = (function () {
	var instance;

	function createInstance() {
			return new object();
	}
	
	class object {
		constructor() {
			this.apps = {
				none: {
					name: 'noneApp',
					class: null
				},
				streamLab: {
					name: 'StreamLab',
					class: StreamLab.getInstance
				}
			};
			
			this.defaultLng = 'ENG';
			this.languages = {
				CZ: 'Česky',
				ENG: 'English',
				Nakashi: 'Nakashi',
				//Sterakdary: 'Sterakdary'
			};
		}

		getApps() {
			return this.apps;
		}

		getLanguages() {
			return this.languages;
		}

		getDefaultLanguage() {
			return this.defaultLng;
		}

		get(key) {
			var result = null;

			switch (key) {
				case 'apps':
					result = this.getApps()
					break;
				case 'languages':
					result = this.getLanguages()
					break;
				case 'defaultLanguage':
					result = this.getDefaultLanguage()
					break;
			
				default: break;
			}

			return result;
		}
	}

	return {
		getInstance: function () {
			if (!instance) {
				instance = createInstance();
			}
			return instance;
		}
	};
})();