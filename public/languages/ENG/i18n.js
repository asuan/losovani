
function getDictionaryENG() {
	var dict = {
		// Page add
		'add': 'Add name',
		'add-title': 'Add name',
		'add-button': 'Add',
		'name': 'Name',
		'err-name': 'Missing name!',
		'priority': 'Priority',
		'err-priority': 'Missing priority',
		'name-add-ok': 'Name is added now.',
		'name-add-fail': 'Fail. Name is not added.',

		// Page list
		'list': 'List names',
		'list-title': 'List names',
		'change-button': 'Save',
		'disable': 'Disable',
		'delete-button': 'Remove',
		'clear-button': 'Remove all',
		'probability': 'Probability',
		'name-change-ok': 'Name is changed.',
		'name-change-fail': 'Fail. Name is not changed.',
		'name-remove-ok': 'Name is removed.',
		'name-remove-fail': 'Fail. Name is not removed.',
		'clear-dialog-title': 'Empty list.',
		'clear-dialog-body': 'Do you try clear list, do you sure?',
		'clear-dialog-delete': 'Delete all names',
		'clear-dialog-cancel': 'Cancel',

		// Page choice
		'choice': 'Choice',
		'choice-title': 'Choice',
		'choice-button': 'Choice',
		'name-choice-ok': 'Congratulations on winning. The name was excluded from the next choice.',
		'name-choice-fail': 'Unfortunately it was not possible to register the winning, choice again.',
		'name-choice-empty': 'Unfortunately list is empty, please add names.',

		// Page help
		'help-title': 'Help',
		'help-info': 'About',
		'help-privacy': 'About privacy',
		'help-privacy-body': '<br/>' +
			'Všechny vyplněné údaje jsou ukládané v prohlížeči uživatele, aplikace žádným způsobem nekomunikuje se serverem. Uložené údaje jsou uloženy trvale a při příštím spuštění aplikace se opět načtou.<br/>',
		'help-add': 'O přidávání donatorů',
		'help-add-body': '<br/>' +
			'<br/>',
		'help-list': 'About listing names',
		'help-list-body': '<br/>' +
			'Výpis je seřazen dle pravděpodobnosti vylosování od nejvyžší, větší pravděpodobnost nezaručuje, že budete vybráni, ale zvyšuje šanci. Pravděpodobnost je počítána ze součtu všech donatů přispěvatele.<br/>' +
			'Červeně jsou označeni přispěvatelé, kteří byli vyřazeni z dalšího losování (viz. sekce "O losování"). Tito uživatelé nejsou započítáni do výpočtu pravděpodobnosti vylosování.<br/>' +
			'Kliknutím na přispěvatele se jeho údaje přepíší do formuláře, kde lze upravit velikost donate, či vyřadit/přidat přispěvatele z/do následujícího losování.<br/>' +
			'Tlačítkem "Uložit" uložíme změněné údaje, tlačítkem "Odstranit" vymaže přispěvatele ze seznamu.<br/>',
		'help-choice': 'About choice',
		'help-choice-body': '<br/>' +
			'Tlačítkem "Losovat" se vylosuje náhodný přispěvatel ze seznamu a následně jej vyřadí z dalších losování. Výše součtu všech donatů zvyšuje pravděpodobnost vylosování, větší pravděpodobnost nezaručuje, že budete vybráni, ale zvyšuje šanci.<br/>',

		// Page settings
		'settings-title': 'Settings',
		'settings-button': 'Save',
		'settings-ok': 'Settings is saved',
		'settings-fail': 'Settings is not unfonantly saved',
		'noneApp': 'None',
		'label-streamApp': 'Stream app',
		'CZK': 'Czech krone',
		'EUR': 'Euro',
		'USD': 'US dolar',
		'twitch': 'Twitch',
		'youtube': 'YouTube',
		'mixer': 'Mixer.com',

		//StreamLab
		'StreamLab': 'StreamLab',
		'StreamLab_Token-label': 'Socket token',
		'StreamLab_Token-err': 'Socket token is not set',
		'StreamLab_Token': 'Socket token',
		'StreamLab_Type-label': 'Event',
		'StreamLab_Currency-label': 'Currency',
		'StreamLab_Platform-label': 'Platform',
		'StreamLab_Bits-label': 'Price 1 Twitch Bit',
		'StreamLab_Bits-err': 'Price must be non zero.',
		'StreamLab_Bits': '',

		//StreamLab
		'StreamLab': 'StreamLab',
		'StreamLab_Token-label': 'Socket token',
		'StreamLab_Token-err': 'Socket token is not set',
		'StreamLab_Token': 'Socket token',
		'StreamLab_Type-label': 'Event',
		'StreamLab_Currency-label': 'Currency',
		'StreamLab_Platform-label': 'Platform',
		'StreamLab_Bits-label': 'Price 1 Twitch Bit',
		'StreamLab_Bits-err': 'Price must be non zero.',
		'StreamLab_Bits': '',

		// Footer
		'label-lng': 'Language',
	};

	return new Promise(function(resolve, reject) {
		var lngParent = 'default';
		var lngPath = 'languages/' + lngParent + '/i18n.js';

		$.getScript(lngPath, function() {
			var dictFunc = 'getDictionary'+ lngParent;
			
			window[dictFunc]()
			.then(function(dictParent) {
				Object.assign(dictParent, dict);
				return resolve(dictParent);
			});
		});
	});
}
