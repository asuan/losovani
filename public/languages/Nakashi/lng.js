var Nakashi = (function () {
    var name = 'Nakashi';
    var instance;

    class lng {
        constructor() {
            // load translations
            loadjscssfile('./i18n.js', 'js');

            this.localization = localizationNakashi;
        }
    }
 
    function createInstance() {
        var object = new lng();

        return object;
    }

    function getInstance() {
        if (!instance) {
            instance = createInstance();
        }
        return instance;
    }
 
    return {
        getInstance: getInstance, 
    };
})()