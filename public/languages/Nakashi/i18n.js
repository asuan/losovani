
function getDictionaryNakashi() {
	var dict = {
		// Page add
		'add': 'Přidat donatora',
		'add-title': 'Přidání donatora',
		'name': 'Donator',
		'err-name': 'Vlož jméno donatora',
		'priority': 'Hodnota donate (bez mezer)',
		'err-priority': 'Vlož hodnotu donate',
		'name-add-ok': 'Donator byl úspěšně zařazen do slosování.',
		'name-add-fail': 'Bohužel nebylo možné donatora zařadit, prosím opakujte.',

		// Page list
		'list': 'Vypsat donatory',
		'list-title': 'Výpis donatorů',
		'name-change-ok': 'Donator byl úspěšně změněn.',
		'name-change-fail': 'Bohužel nebylo možné zapsat změny u donatora.',
		'name-remove-ok': 'Donator byl odebrán z losování.',
		'name-remove-fail': 'Bohužel nebylo možné donatora odebrat.',
		'clear-dialog-title': 'Smazat seznam!',
		'clear-dialog-body': 'Opravdu chcete vymazat všechny donatory?',
		'clear-dialog-delete': 'Všichni jsou mrtví, Dejve!',
		'clear-dialog-cancel': 'Zrušit',

		// Page choice
		'name-choice-ok': 'Gratulujeme k výhře. Jméno bylo vyřazeno z dalších losování.',
		'name-choice-fail': 'Bohužel nebylo možné zapsat výhru, losujte znovu.',
		'name-choice-empty': 'Bohužel není z koho vybírat, nejdříve přidejte donatory.',

		// Page help
		'help-add': 'O přidávání donatorů',
		'help-add-body': '<br/>' +
				'<br/>',
		'help-list': 'O výpisu donatorů',
		'help-list-body': '<br/>' +
				'Výpis je seřazen dle pravděpodobnosti vylosování od nejvyžší, větší pravděpodobnost nezaručuje, že budete vybráni, ale zvyšuje šanci. Pravděpodobnost je počítána ze součtu všech donatů přispěvatele.<br/>' +
				'Červeně jsou označeni přispěvatelé, kteří byli vyřazeni z dalšího losování (viz. sekce "O losování"). Tito uživatelé nejsou započítáni do výpočtu pravděpodobnosti vylosování.<br/>' +
				'Kliknutím na přispěvatele se jeho údaje přepíší do formuláře, kde lze upravit velikost donate, či vyřadit/přidat přispěvatele z/do následujícího losování.<br/>' +
				'Tlačítkem "Uložit" uložíme změněné údaje, tlačítkem "Odstranit" vymaže přispěvatele ze seznamu.<br/>',
		'help-choice': 'O losování',
		'help-choice-body': '<br/>' +
				'Tlačítkem "Losovat" se vylosuje náhodný přispěvatel ze seznamu a následně jej vyřadí z dalších losování. Výše součtu všech donatů zvyšuje pravděpodobnost vylosování, větší pravděpodobnost nezaručuje, že budete vybráni, ale zvyšuje šanci.<br/>',
	};
	
	return new Promise(function(resolve, reject) {
		var lngParent = 'CZ';
		var lngPath = 'languages/' + lngParent + '/i18n.js';

		$.getScript(lngPath, function() {
			var dictFunc = 'getDictionary'+ lngParent;
			
			window[dictFunc]()
			.then(function(dictParent) {
				Object.assign(dictParent, dict);
				return resolve(dictParent);
			});
		});
	});
}
