
function getDictionarydefault() {
	var dict = {
		'help-info': 'About',
		'help-info-body': '<br/>Verze 1.5.4 release day 2020-05-06<br/><br/>' +
			'Copyright (c) 2020 AsuanWeb, MIT License<br/>' +
			'Open source app<br/><br/>' +
			'Change log: <a href="https://gitlab.com/asuan/losovani/-/blob/master/public/changelog.log">https://gitlab.com/asuan/losovani/-/blob/master/public/changelog.log</a><br/>' +
			'Source code: <a href="https://gitlab.com/asuan/losovani">https://gitlab.com/asuan/losovani</a><br/>' +
			'Feedback: <a href="https://gitlab.com/asuan/losovani/-/issues">https://gitlab.com/asuan/losovani/-/issues</a> or <a href="mailto:asuanweb@gmail.com">asuanweb@gmail.com</a>',
			'twitch': 'Twitch',
			'youtube': 'YouTube',
			'mixer': 'Mixer.com',
    };
    
    return Promise.resolve(dict);
}
