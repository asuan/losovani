
function getDictionaryCZ() {
	var dict = {
		// Page add
		'add': 'Přidat jméno',
		'add-title': 'Přidání jména',
		'add-button': 'Přidat',
		'name': 'Jméno',
		'err-name': 'Vlož jméno',
		'priority': 'Oblíbenost jména (1-100)',
		'err-priority': 'Vlož oblíbenost jména (1-100)',
		'name-add-ok': 'Jméno bylo úspěšně zařazeno.',
		'name-add-fail': 'Bohužel nebylo možné jméno zařadit, prosím opakujte.',

		// Page list
		'list': 'Vypsat jména',
		'list-title': 'Výpis jmen',
		'change-button': 'Upravit',
		'disable': 'Vyřadit',
		'delete-button': 'Odstranit',
		'clear-button': 'Odstranit vše',
		'probability': 'Pravděpodobnost výběru',
		'name-change-ok': 'Jméno bylo úspěšně změněno.',
		'name-change-fail': 'Bohužel nebylo možné zapsat změny.',
		'name-remove-ok': 'Jméno bylo odebráno.',
		'name-remove-fail': 'Bohužel nebylo možné jméno odebrat.',
		'clear-dialog-title': 'Smazat seznam!',
		'clear-dialog-body': 'Opravdu chcete vymazat všechna jména?',
		'clear-dialog-delete': 'Smazat vše',
		'clear-dialog-cancel': 'Zrušit',

		// Page choice
		'choice': 'Losovat',
		'choice-title': 'Losování',
		'choice-button': 'Losovat',
		'name-choice-ok': 'Jméno bylo vyřazeno z dalších losování.',
		'name-choice-fail': 'Bohužel nebylo možné vybrat, losujte znovu.',
		'name-choice-empty': 'Bohužel není z koho vybírat, nejdříve přidejte jména.',

		// Page help
		'help-title': 'Nápověda',
		'help-info': 'O aplikaci',
		'help-privacy': 'O ochraně soukromí',
		'help-privacy-body': '<br/>' +
			'Všechny vyplněné údaje jsou ukládané v prohlížeči uživatele, aplikace žádným způsobem nekomunikuje se serverem. Uložené údaje jsou uloženy trvale a při příštím spuštění aplikace se opět načtou.<br/>',
		'help-add': 'O přidávání jmen',
		'help-add-body': '<br/>' +
						'<br/>',
		'help-list': 'O výpisu jmen',
		'help-list-body': '<br/>' +
			'Výpis je seřazen dle pravděpodobnosti vylosování od nejvyžší, větší pravděpodobnost nezaručuje, že bude jméno vybráno, ale zvyšuje šanci. Pravděpodobnost je počítána dle zadané oblíbenosti.<br/>' +
			'Červeně jsou označena jména, která byla vyřazena z dalšího losování (viz. sekce "O losování"). Tato jména nejsou započítána do výpočtu pravděpodobnosti vylosování.<br/>' +
			'Kliknutím na jméno se jeho údaje přepíší do formuláře, kde lze upravit oblíbenost, či vyřadit/přidat jméno z/do následujícího losování.<br/>' +
			'Tlačítkem "Uložit" uložíme změněné údaje, tlačítkem "Odstranit" vymaže přispěvatele ze seznamu.<br/>',
		'help-choice': 'O losování',
		'help-choice-body': '<br/>' +
			'Tlačítkem "Losovat" se vylosuje náhodné jméno ze seznamu a následně jej vyřadí z dalších losování. Oblíbenost zvyšuje pravděpodobnost vylosování, větší pravděpodobnost nezaručuje, že bude jméno vybráno, ale zvyšuje šanci.<br/>',

		// Page settings
		'settings-title': 'Nastavení',
		'settings-button': 'Uložit',
		'settings-ok': 'Nastavení úspěšně uloženo',
		'settings-fail': 'Nastavení se bohužel nepodařilo uložit',
		'noneApp': 'Žádná',
		'label-streamApp': 'Streamovací aplikace',
		'CZK': 'Česká koruna',
		'EUR': 'Euro',
		'USD': 'US dolar',

		//StreamLab
		'StreamLab': 'StreamLab',
		'StreamLab_Token-label': 'Socket token',
		'StreamLab_Token-err': 'Socket token není nastaven.',
		'StreamLab_Token': 'Socket token',
		'StreamLab_Type-label': 'Událost',
		'StreamLab_Currency-label': 'Měna',
		'StreamLab_Platform-label': 'Platforma',
		'StreamLab_Bits-label': 'Cena v € za jeden Twitch Bit',
		'StreamLab_Bits-err': 'Musí být kladné číslo',
		'StreamLab_Bits': '',
		'donation': 'Příspěvky a dary',
		'follow': 'Noví sledující',
		'subscription': 'Předplatné',

		// Footer
		'label-lng': 'Zobrazení',
	};

	return new Promise(function(resolve, reject) {
		var lngParent = 'default';
		var lngPath = 'languages/' + lngParent + '/i18n.js';

		$.getScript(lngPath, function() {
			var dictFunc = 'getDictionary'+ lngParent;
			
			window[dictFunc]()
			.then(function(dictParent) {
				Object.assign(dictParent, dict);
				return resolve(dictParent);
			});
		});
	});
}
